variable "prefix" {
  default = "raad"
}
variable "project" {
  default = "recipe-app-api-devops"
}

variable "contact" {
  default = "uei13234@rmd.ac.in"
}
