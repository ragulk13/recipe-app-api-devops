terraform {
  backend "s3" {
    bucket         = "gitlab-gullu-terrastate-bucket"
    region         = "us-east-1"
    key            = "recipe-app.tfstate"
    encrypt        = false
    dynamodb_table = "recipe-app-api-devops-tf-state-lock"
  }

}

provider "aws" {
  region  = "us-east-1"
  version = "~> 2.50.0"
}
locals {
  prefix = "${var.prefix}-${terraform.workspace}"
  common_tags = {
    Environment = terraform.workspace
    Project     = var.project
    Owner       = var.contact
    ManagedBy   = "Terraform"
  }
}
